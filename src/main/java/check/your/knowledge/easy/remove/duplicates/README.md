#### Problem Statement
```
A function that takes an integer array in sorted order and returns number of unique integers.
To find this value, we have to modify input array such that all unique numbers come in front.
```

##### Example
```
input = 1 1 2 2 3
output = 3

Explanation:
Input has three unique numbers which is why output is 3. However input array should be changed to: 1 2 3 _ _.
First three digits should be replaced with unique digits after which input array can have anything. 

```             