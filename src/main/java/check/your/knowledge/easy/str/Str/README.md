#### Problem Statement
```
A function that takes two strings as input, haystack and needle.
We need to determine if haystack contains needle. If yes return the starting index, else return -1
```

##### Example
```
input
    haystack: banana
    needle: na
output = 2

Explanation:
Haystack contains two 'na' however first occurence is at index 2.

input
    haystack: solution
    needle: to
output = -1

Explanation:
Haystack doesn't contain to as a substring 

```             