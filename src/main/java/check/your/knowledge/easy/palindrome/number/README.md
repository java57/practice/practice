#### Problem Statement
```
A function that takes an integer value and checks if the number is a palindrome or not.
For this question we assume that all numbers less than 10 are palindrome.
```

##### Example
```
input = 121
output = true

input = 1234
output = false
```             