#### Problem Statement
```
A function that takes a string comprises of different brackets '(', '{', etc. and checks if it is made up of valid parenthesis or not.
We assume valid parenthesis when each open bracket has closed bracket too and in correct order
```

##### Example
```
input = (){}
output = true

input = ({)}
output = false
```             